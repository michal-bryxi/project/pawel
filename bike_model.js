const {DataTypes} = require('sequelize');

module.exports = (sequelize) => {
  const bikeModel = sequelize.define("bike_model", {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false,
    },
  });
  return bikeModel;
};