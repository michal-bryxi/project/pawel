const express = require('express')
const router = express.Router()
const { Sequelize } = require('sequelize');

router.use('/:database/*', function(req, res, next){
  let sequelize;

  switch(req.params.database) {
    case 'cool-db':
      sequelize = new Sequelize({
        dialect: 'sqlite',
        storage: `./cool-db.sqlite`
      });
      break
    case 'not-so-cool-db':
      sequelize = new Sequelize({
        dialect: 'sqlite',
        storage: `./not-so-cool.sqlite`
      });
      break;
    default:
      sequelize = new Sequelize({
        dialect: 'sqlite',
        storage: `./something-else.sqlite`
      });
      break;
  }

  // const sequelize = `connected to ${req.params.database}`; // this is just for example

  req.db = sequelize; // this way I can pass it to next app.use() call

  next(); // this way routing won't stop here
});

module.exports = router;